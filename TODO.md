# TODO

## gcc 9

Dans le cas de gcc 9 appliquer le [patch](https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.2.6/0002-UBUNTU-SAUCE-kbuild-add-fcf-protection-none-when-usi.patch).

Pour connaitre la version de gcc : `gcc -dumpversion`
