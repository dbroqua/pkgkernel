#! /bin/bash

# Some colors
GREEN='\033[0;32m'
RED='\033[0;31m'
DEFAULT='\033[0m'

# Defaults values
PREFIX='linux-'
EXTENSION='.tar.xz'
MAINLINE=1
SHOW_MENU_CONFIG=0

# Function to script script if step fail
isPreviousStepOk () {
    if [ $? -ne 0 ]; then
        echo "${RED} Error!${DEFAULT}" 
        exit 1
    fi
}

# Get arguments from command line
while [ $# -gt 0 ]; do
    case "$1" in
        --help)
            printf "****************************************************************************************************\n"
            printf "* pkgkernel-auto: Usage\n"
            printf "* --menu: show kernel menu\n"
            printf "* --archive <path to local kernel archive>: Use local kernel archive\n"
            printf "* --extension <archive extention>: if not a archive extension is not tar.xz (tar.xz or tar.gz)\n"
            printf "* --rc: Get RC version instead of mainline\n"
            printf "****************************************************************************************************\n"
            printf "Example with RC kernel: \n"
            printf "pkgkernel-auto.sh --archive /home/dbroqua/Downloads/linux-5.7-rc4.tar.gz --extension tar.gz \n"
            printf "****************************************************************************************************\n"
            exit 1
        ;;
        --menu)
            SHOW_MENU_CONFIG=1
        ;;
        --archive)
            ARCHIVE=$2
            shift
        ;;
        --rc)
            MAINLINE=0
        ;;
        --extension)
            EXTENSION=$2
            shift
        ;;
        *)
            printf "***************************\n"
            printf "* Error: Invalid argument.*\n"
            printf "***************************\n"
            exit 1
    esac
    shift
done

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 1>&2
   sudo $0
   exit 1
fi

# Install build dependancies
apt install -y \
    build-essential \
    fakeroot \
    dpkg-dev \
    perl \
    libssl-dev \
    bc \
    gnupg \
    dirmngr \
    libelf-dev \
    flex \
    bison \
    libncurses-dev \
    rsync \
    git \
    curl

if [ "${ARCHIVE}" != "" ] ; then
    FILE=`basename "$ARCHIVE"`
    KERNEL=`echo ${FILE}|sed 's/linux-//'|sed "s/\.${EXTENSION}//"`
else 
    if [ ${MAINLINE} -eq 1 ] ; then 
        # Get last kernel version
        INFO=`curl -Ls https://www.kernel.org/ | perl -lne 'BEGIN{$/=""} print "$1 $2" if \
            /latest_link.*?<a.*?href=\"(.*?)\".*?>(.*?)</s'`
        # Extract some values from kernel name
        KERNEL=`echo ${INFO} | cut -d' ' -f 2`
        URL=`echo ${INFO} | cut -d' ' -f 1`
        FILE=`echo ${URL}|rev|cut -d'/' -f 1 | rev`
    else 
        INFO=`curl -sS https://www.kernel.org/ |grep href |cut -f2 -d\" |grep rc |grep git |grep tar`
        EXTENSION="tar.gz"
        URL=${INFO}
        FILE=`basename "$INFO"`
        KERNEL=`echo ${FILE}|sed 's/linux-//'|sed "s/\.${EXTENSION}//"`
    fi
fi

# Directory to store tmp files
DIR="/usr/src/${KERNEL}"

# Make directory in /usr/src/
mkdir "${DIR}"

# Go to build dir
cd "${DIR}" || exit

# Get kernel archive
if [ "${ARCHIVE}" == "" ] ; then
    wget ${URL}
    isPreviousStepOk
fi

# Extract kernel archive
if [ "${EXTENSION}" == '.tar.xz' ] ; then
    tar xavf "${FILE}"
else 
    if [ "${ARCHIVE}" == "" ] ; then
        tar xzvf "${FILE}"
    else
        tar xzvf "${ARCHIVE}" -C ${DIR}
    fi
fi
isPreviousStepOk

# Enter kernel directory
cd "linux-${KERNEL}"
isPreviousStepOk

# Get old config
make olddefconfig
isPreviousStepOk

# Show kernel menu
if [ ${SHOW_MENU_CONFIG} -eq 1 ]; then
    make menuconfig
fi

# Some optimizations
# ./scripts/config -d CONFIG_MODULE_SIG_ALL -d CONFIG_MODULE_SIG_KEY -d CONFIG_SYSTEM_TRUSTED_KEYS
# isPreviousStepOk
./scripts/config -d CONFIG_DEBUG_INFO
isPreviousStepOk

# Compile kernel
make deb-pkg -j"$(nproc)" LOCALVERSION=-"$(dpkg --print-architecture)" \
    KDEB_PKGVERSION="$(make kernelversion)-1"
isPreviousStepOk

# Optimization
make bindeb-pkg -j"$(nproc)" LOCALVERSION=-"$(dpkg --print-architecture)" \
    KDEB_PKGVERSION="$(make kernelversion)-1"
isPreviousStepOk

# Go to parent directory
cd ..

# Install new packages
dpkg -i ./*${KERNEL}*.deb
isPreviousStepOk

# Update acpi_call
git clone git://github.com/teleshoes/acpi_call.git /tmp/acpi_call
isPreviousStepOk
cd /tmp/acpi_call
make
isPreviousStepOk
make install
isPreviousStepOk
cd /tmp

# Clean directories
rm -rf /tmp/acpi_call ${DIR}

echo -e "${GREEN} ALL DONE!\n"
