#! /bin/bash

# Gentoo version

# Some colors
GREEN='\033[0;32m'
RED='\033[0;31m'
DEFAULT='\033[0m'

# Defaults values
DIR="/usr/src/"
PREFIX='linux-'
EXTENSION='.tar.xz'
MAINLINE=1
CURRENT_VERSION=`uname -r`

# Function to script script if step fail
isPreviousStepOk () {
  if [ $? -ne 0 ]; then
    echo -e "${RED} Error!${DEFAULT}"
    exit 1
  fi
}

if [[ $EUID -ne 0 ]]; then
  echo "This script must be run as root" 1>&2
  sudo $0
  exit 1
fi

# Get last kernel version
INFO=`curl -Ls https://www.kernel.org/ | perl -lne 'BEGIN{$/=""} print "$1 $2" if \
    /latest_link.*?<a.*?href=\"(.*?)\".*?>(.*?)</s'`

# Extract some values from kernel name
KERNEL=`echo ${INFO} | cut -d' ' -f 2`
URL=`echo ${INFO} | cut -d' ' -f 1`
FILE=`echo ${URL}|rev|cut -d'/' -f 1 | rev`

if [ "${CURRENT_VERSION}" == "${KERNEL}" ]; then
  echo -e "${RED}You already have the latest version${DEFAULT}"
  exit 1
fi

# Go to build dir
cd ${DIR} || exit

# Get kernel archive
wget ${URL}
isPreviousStepOk

# Extract kernel archive
tar xavf "${FILE}"
isPreviousStepOk

# Updated symlink
ln -snf linux-${KERNEL} linux
isPreviousStepOk

# Enter kernel directory

cd linux
isPreviousStepOk

# Get old config
make oldconfig
isPreviousStepOk

# Build kernel
make -j$(nproc)

# Build modules
make modules_install

# Install kernel
make install

# Update grub
grub-mkconfig -o /boot/grub/grub.cfg

# Rebuild modules
emerge --ask --verbose @module-rebuild

# Go to parent directory
cd ..

# Remove archive
rm ${FILE}

echo -e "${GREEN} ALL DONE!\n"
