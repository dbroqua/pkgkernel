# pkgkernel

Simple outils permettant de compiler le kernel sur GNU/Debian et Gentoo (ou autre).

Based on https://debian-facile.org/doc:systeme:kernel:compiler

* pkgkernel.sh : GNU/Debian
* mrgkernel.sh : Gentoo / Générique
